#!/bin/bash

# Utilisation avec la tablette X-TOUCH MINI de Behringer
# Version avec la tablette en mode Mackie

# Aide : 
# - Pour PulseAudio, sink = output, source = input

# 0. On va identifier les équipements physiques (micro + sortie)
# Identification de l'identifiant de la platine MIDI
amidi_hw=$(amidi --list-devices | grep "X-TOUCH" | cut -d " " -f 3)
# Le Micro : pactl list short sources
#microphone="alsa_input.usb-ZOOM_Corporation_H1n_000000000000-00.iec958-stereo"
microphone="alsa_input.usb-Logitech_PRO_X_000000000000-00.mono-fallback"
# Les speakers : pactl list short sinks; "combined" est une sortie virtuelle vers tous les périph de sortie
#speakers="alsa_output.pci-0000_0b_00.4.analog-stereo"
#speakers="alsa_output.usb-Logitech_PRO_X_000000000000-00.iec958-stereo"
speakers="combined"
# Camera
camera_dev=$(v4l2-ctl --list-devices 2>/dev/null | grep -A1 "PC-W3" | tail -1 | tr -d "\t")
camera_dev="/dev/video0"

# Est-ce qu'on évite le wiring Pulse
if [ $# -ge 1 ] 
then
	echo "WARNING : Starting without PULSE wiring because of args $*"
	with_pulse=0
else
	with_pulse=1
fi

wire_pulse() {
	# 1. On remet PulseAudio à zéro, et on lui laisse le temps de redémarrer
	echo "Rebooting Pulseaudio"
	pulseaudio -k
	sleep 3

	# 2. Mise en place d'un anti-echo pour que quand le son sort sur les hauts parleurs, le micro ignore (anti larsen / echo)
	# ça créé un source pour le micro (src_mic_echocancel) et une sortie sink_main qu'on redirigera vers les hauts parleurs
	echo "Setting up echo cancellation"
	pactl load-module module-echo-cancel use_master_format=1 aec_method=webrtc \
		aec_args="analog_gain_control=0\\ digital_gain_control=1\\ experimental_agc=1\\ noise_suppression=1\\ voice_detection=1\\ extended_filter=1" \
		source_master="$microphone" source_name=src_mic_echocancel source_properties=device.description=src_mic_echocancel \
		sink_master="$speakers"     sink_name=sink_main            sink_properties=device.description=sink_main
	
	# 3. On créé :
	# - sink_effects vers laquelle sont poussés tous les sons qu'on veut insérer dans le micro (Soundboard)
	# - sink_effects_and_mic, c'est la sortie (sink) commune vers laquelle on pousse sink_effects + le micro physique
	echo "Creating virtual output devices"
	pactl load-module module-null-sink sink_name=sink_effects  			sink_properties=device.description=sink_effects
	pactl load-module module-null-sink sink_name=sink_effects_and_mic 	sink_properties=device.description=sink_effects_and_mic

	# 4. Quelques logiciels (chromium) ne reconnaissent pas les périphériques monitor comme un vrai micro
	# alors on créé une source standard (src_main) et on lui attache le monitor de sink_effects_and_mic
	echo "Creating remaps"
	pactl load-module module-remap-source master=sink_effects_and_mic.monitor \
		source_name=src_main source_properties="device.description=MainInput"

	# 5. Par défault, on enregistre micro+fx depuis src_main (qui est le monitor du sink_effects_and_mic)
	# et on sort tout dans sink_main (créé lors de l'anti-echo)
	echo "Setting default devices"
	pactl set-default-source src_main
	pactl set-default-sink   sink_main
	echo "Prevent sink/sources to be muted on start"
	pactl set-sink-mute sink_main 0
	pactl set-sink-mute sink_effects 0
	pactl set-sink-mute sink_effects_and_mic 0
	pactl set-source-mute src_main 0
	pactl set-source-mute src_mic_echocancel 0
	pactl set-source-mute sink_main.monitor 0
	pactl set-source-mute sink_effects.monitor 0
	pactl set-source-mute sink_effects_and_mic.monitor 0
	
	
	# 6. On créé les loopback pour entendre dans les hauts parleurs, c'est le wiring effectif
	# On pousse le son du micro (anti-echoed) vers le sink_effects_and_mic
	# On pousse tous les sons envoyés vers sink_effects dans sink_effects_and_mic
	# On pousse tous les sons envoyés vers sink_effects dans les hauts parleurs
	echo "Creating loopbacks"
	pactl load-module module-loopback latency_msec=60 adjust_time=6 source=src_mic_echocancel   sink=sink_effects_and_mic
	pactl load-module module-loopback latency_msec=60 adjust_time=6 source=sink_effects.monitor sink=sink_effects_and_mic
	pactl load-module module-loopback latency_msec=60 adjust_time=6 source=sink_effects.monitor sink=sink_main

	# Mise à dispo d'un loopback pour entendre le micro, mais on le mute par defaut
	#echo "Creating loopbacks"
	#pactl load-module module-loopback latency_msec=60 adjust_time=6 source=src_mic_echocancel   sink=sink_main

	# À la fin, le son du micro et le son des FX (ou toute app qui prend pour carte de sortie sink_effects)
	# vont donc dans ink_effects_and_mic. En (4), on prend le retour (monitor) pour en faire une entrée à part entière (src_main)
	# pour Chrome.
	# La carte son du système par défaut (pour toutes les app qui ne sont pas soundboard) est sink_main
	# Le micro que vous donnez aux applications est src_main
}

# Init d'un potar : lights off
reset_potar () {
	# Led éteinte
	amidi -p "$amidi_hw" -S "BA $1 00";
	echo amidi -p "$amidi_hw" -S "BA $1 00";
}

# Init d'un bouton : lights off
reset_button () {
	# Led éteinte
	amidi -p "$amidi_hw" -S "90 $1 00";
	echo amidi -p "$amidi_hw" -S "90 $1 00";
}

# Par défaut, ma webcam et/ou chromium activent des réglages auto, qu'on désactive ici
# On va stocker aussi les valeurs dans des variables pour pouvoir les modifier en relatif
# sans avoir à appelr v4l2-ctl à chaque mouvement de potar
default_cam_options() {
	default_exposure_absolute=1205
	current_exposure_absolute=$default_exposure_absolute
	
	default_contrast=113
	current_contrast=$default_contrast
	
	default_saturation=141
	current_saturation=$default_saturation
	
	default_gain=47
	current_gain=$default_gain
	
	default_white_balance_temperature=4450
	current_white_balance_temperature=$default_white_balance_temperature
	
	default_sharpness=128
	current_sharpness=$default_sharpness
	
	default_brightness=128
	current_brightness=$default_brightness
}

# Tool : fonction d'application des paramètres à la cam
apply_cam_options () {
	v4l2-ctl -d $camera_dev \
		--set-ctrl=exposure_auto_priority=0 \
		--set-ctrl=exposure_auto=2 \
		--set-ctrl=white_balance_temperature_auto=0 \
		--set-ctrl=exposure_absolute=$current_exposure_absolute \
		--set-ctrl=contrast=$current_contrast \
		--set-ctrl=saturation=$current_saturation \
		--set-ctrl=gain=$current_gain \
		--set-ctrl=white_balance_temperature=$current_white_balance_temperature \
		--set-ctrl=sharpness=$current_sharpness \
		--set-ctrl=brightness=$current_brightness
}

# Init de la cam
init_camera() {
	default_cam_options
	apply_cam_options
}

# Init de la camera, tous les potars et boutons
reset_all () {
	echo "Resetting all devices"
	
	echo "Resetting webcam"
	init_camera

	echo "Resetting potars"
	# Reset des potars en bouclant sur leur index en hexa
	for i in 1 2 3 4 5 6 7 8
	do
		reset_potar $i
	done

	echo "Resetting buttons"
	# Reset des boutons en bouclant sur leur index en hexa
	# attention en Relative les index de LED sont pas les même que les index de boutons
	for i in 0 1 2 3 4 5 6 7 8 9 A B C D E F
	do
		reset_button $i
	done
}

# Tool : décoder les valeurs relatives des potars
decode_relative_potar() {
	case "$1" in
		65 )	potar_change_value=2;;
		66 )	potar_change_value=4;;
		67 )	potar_change_value=8;;
		68 )	potar_change_value=16;;
		69 )	potar_change_value=32;;
		70 )	potar_change_value=64;;
		
		63 )	potar_change_value=-2;;
		62 )	potar_change_value=-4;;
		61 )	potar_change_value=-8;;
		60 )	potar_change_value=-16;;
		59 )	potar_change_value=-32;;
		58 )	potar_change_value=-64;;
	esac	
}

# Tool : Fonction de jeu de son (vers le sink_effects)
play () {
	sound=$(dirname $0)/sounds-enabled/$1
	echo "Playing $sound"
	#mplayer -quiet -noconsolecontrols $1 &
	killall paplay 2>/dev/null || paplay -d sink_effects $sound &
}

# Routine principale, qui contient le loop pour le midi
launch () {
	# On affiche la date pour la retrouver dans les logs
	echo "$(date) : Launching $0 in $(pwd)"
		
	reset_all

	if [[ with_pulse -eq 0 ]]
	then
		echo "Skipping PULSE wiring"
	else
		wire_pulse
	fi

	# Loop Midi
	aseqdump -p "X-TOUCH MINI" | while IFS=" ," read src ev1 ev2 ch label1 data1 label2 data2 rest; do
		# echo "DEBUG_RAW src:$src ev1:$ev1 ev2:$ev2 ch:$ch label1:$label1 data1:$data1 label2:$label2 data2:$data2 rest:$rest"
		
		# Réaction à chaque event
		case "$ev1 $ev2" in

			# Bouton Appuyé
			"Note off" )
				case "$data1" in
					# Rangée 1 : Soundboard
					8 ) play 1.wav ;;
					9 ) play 2.wav ;;
					10 ) play 3.wav ;;
					11 ) play 4.wav ;;
					12 ) play 5.wav ;;
					13 ) play 6.wav ;;
					14 ) play 7.wav ;;
					55 ) play 8.wav ;;

					# Rangée 2
					16 ) play 9.wav ;;
					17 ) play 10.wav ;;
					18 ) play 11.wav ;;
					19 ) play 12.wav ;;
					20 ) play 13.wav ;;
					21 ) ;;
					22 ) echo "Launch or focus Spotify" ; wmctrl -a Spotify || /snap/bin/spotify & ;;
					23 )
						pactl set-source-mute sink_effects_and_mic.monitor toggle 
						echo pactl set-source-mute sink_effects_and_mic.monitor toggle 
						pactl get-source-mute sink_effects_and_mic.monitor
						echo pactl get-source-mute sink_effects_and_mic.monitor
						;;
					
					# Potar 1 : reset de la caméra
					0 )
						default_cam_options
						apply_cam_options
						;;
				esac
			;;
			
			# Potar Tourné
			"Control change" )
				decode_relative_potar $data2
				#echo "DEBUG Potar : $data1 $data2 $potar_change_value" ; 

				case "$data1" in
					# Min/Max à checker avec v4l2-ctl -d /dev/video0 -l
					1 ) 
						# Je fais un x10 sur la valeur à ajouter parce que le range de exposition est très large
						current_exposure_absolute=$(($current_exposure_absolute+potar_change_value*10))
						if [ $current_exposure_absolute -lt 5 ]; then current_exposure_absolute=5; fi
						if [ $current_exposure_absolute -gt 2500 ]; then current_exposure_absolute=2500; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=exposure_absolute=$current_exposure_absolute \
						;;
					2 ) 
						current_contrast=$(($current_contrast+potar_change_value))
						if [ $current_contrast -lt 1 ]; then current_contrast=1; fi
						if [ $current_contrast -gt 255 ]; then current_contrast=255; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=contrast=$current_contrast \
						;;
					3 ) 
						current_saturation=$(($current_saturation+potar_change_value)) 
						if [ $current_saturation -lt 1 ]; then current_saturation=1; fi
						if [ $current_saturation -gt 255 ]; then current_saturation=255; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=saturation=$current_saturation \
						;;
					4 ) 
						current_gain=$(($current_gain+potar_change_value)) 
						if [ $current_gain -lt 1 ]; then current_gain=1; fi
						if [ $current_gain -gt 100 ]; then current_gain=100; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=gain=$current_gain \
						;;
					5 ) 
						# Je fais un x50 sur la valeur à ajouter parce que le range de white_balance est très large
						current_white_balance_temperature=$(($current_white_balance_temperature+potar_change_value*50)) 
						if [ $current_white_balance_temperature -lt 2800 ]; then current_white_balance_temperature=2800; fi
						if [ $current_white_balance_temperature -gt 6500 ]; then current_white_balance_temperature=6500; fi
						
						# Chromium réactive l'expo auto de la camra, donc on doit aussi la redésactiver
						v4l2-ctl -d $camera_dev \
							--set-ctrl=white_balance_temperature_auto=0 \
							--set-ctrl=white_balance_temperature=$current_white_balance_temperature \
						;;
					6 ) 
						current_sharpness=$(($current_sharpness+potar_change_value)) 
						if [ $current_sharpness -lt 1 ]; then current_sharpness=1; fi
						if [ $current_sharpness -gt 255 ]; then current_sharpness=255; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=sharpness=$current_sharpness \
						;;
					7 ) 
						current_brightness=$(($current_brightness+potar_change_value))
						if [ $current_brightness -lt 1 ]; then current_brightness=1; fi
						if [ $current_brightness -gt 255 ]; then current_brightness=255; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=brightness=$current_brightness
						;;
					8 ) 
						;;

				esac
				
			;;
	    esac

		# Allumer les LEDs si besoin, attention en Relative les index de LED sont pas les même que les index de boutons
		(wmctrl -l | grep Spotify > /dev/null) && amidi -p "$amidi_hw" -S "90 0E 01" || amidi -p "$amidi_hw" -S "90 0E 00";
		
		# LED du micro (LAYER A) allumé, attention en Relative les index de LED sont pas les même que les index de boutons
		mic_open=$(pactl get-source-mute sink_effects_and_mic.monitor | grep oui > /dev/null ; echo $?)
		if [ "$mic_open" = 0 ]
		then
			echo amidi -p "$amidi_hw" -S '90 23 7F'
			amidi -p "$amidi_hw" -S '90 0F 01'
		else
			echo amidi -p "$amidi_hw" -S '90 23 00'
			amidi -p "$amidi_hw" -S '90 0F 00'
		fi			
	done
}

# On lance en gardant trace des outputs en guise de logs
launch | tee $(dirname $0)/logs/$(date +%Y%m%d-%H%M).log
