# Directory of usable sounds

Prefer having WAV files in this directory.

You can convert an MP3 to a WAV using this command :

mpg321 -g 10 -n 100 -w converted.wav original.mp3

`-g` sets the gain, for manual normalization of your sounds

`-n 100` only extract the 100 first frames of the MP3