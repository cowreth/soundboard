# Directory of used sounds

Sounds are by default numbered [1-8].wav.

I prefer to use symlinks rather than copying / renaming files.

`ln -f -s ../sounds-avalaible/converted.wav 1.wav`