#!/bin/bash

# Utilisation avec la tablette X-TOUCH MINI de Behringer

# Aide : 
# - Pour PulseAudio, sink = output, source = input

# 0. On va identifier les équipements physiques (micro + sortie)
# Identification de l'identifiant de la platine MIDI
amidi_hw=$(amidi --list-devices | grep "X-TOUCH" | cut -d " " -f 3)
# Le Micro : pactl list short sources
microphone="alsa_input.usb-ZOOM_Corporation_H1n_000000000000-00.iec958-stereo"
microphone="alsa_input.usb-Logitech_PRO_X_000000000000-00.mono-fallback"
# Les speakers : pactl list short sinks
speakers="alsa_output.pci-0000_0b_00.4.analog-stereo"
speakers="alsa_output.usb-Logitech_PRO_X_000000000000-00.iec958-stereo"
speakers="combined"

wire_pulse() {
	# 1. On remet PulseAudio à zéro, et on lui laisse le temps de redémarrer
	echo "Rebooting Pulseaudio"
	pulseaudio -k
	sleep 3

	# 2. Mise en place d'un anti-echo pour que quand le son sort sur les hauts parleurs, le micro ignore (anti larsen / echo)
	# ça créé un source pour le micro (src_mic_echocancel) et une sortie sink_main qu'on redirigera vers les hauts parleurs
	echo "Setting up echo cancellation"
	pactl load-module module-echo-cancel use_master_format=1 rate=48000 aec_method=webrtc \
		aec_args="analog_gain_control=0\\ digital_gain_control=1\\ experimental_agc=1\\ noise_suppression=1\\ voice_detection=1\\ extended_filter=1" \
		source_master="$microphone" source_name=src_mic_echocancel source_properties=device.description=src_mic_echocancel \
		sink_master="$speakers"     sink_name=sink_main            sink_properties=device.description=sink_main
	
	# 3. On créé :
	# - sink_effects vers laquelle sont poussés tous les sons qu'on veut insérer dans le micro (Soundboard)
	# - sink_effects_and_mic, c'est la sortie (sink) commune vers laquelle on pousse sink_effects + le micro physique
	echo "Creating virtual output devices"
	pactl load-module module-null-sink sink_name=sink_effects  			sink_properties=device.description=sink_effects
	pactl load-module module-null-sink sink_name=sink_effects_and_mic 	sink_properties=device.description=sink_effects_and_mic

	# 4. Quelques logiciels (chromium) ne reconnaissent pas les périphériques monitor comme un vrai micro
	# alors on créé une source standard (src_main) et on lui attache le monitor de sink_effects_and_mic
	echo "Creating remaps"
	pactl load-module module-remap-source master=sink_effects_and_mic.monitor \
		source_name=src_main source_properties="device.description=MainInput"

	# 5. Par défault, on enregistre micro+fx depuis src_main (qui est le monitor du sink_effects_and_mic)
	# et on sort tout dans sink_main (créé lors de l'anti-echo)
	echo "Setting default devices"
	pactl set-default-source src_main
	pactl set-default-sink   sink_main
	echo "Prevent sink/sources to be muted on start"
	pactl set-sink-mute sink_main 0
	pactl set-sink-mute sink_effects 0
	pactl set-sink-mute sink_effects_and_mic 0
	pactl set-source-mute src_main 0
	pactl set-source-mute src_mic_echocancel 0
	pactl set-source-mute sink_main.monitor 0
	pactl set-source-mute sink_effects.monitor 0
	pactl set-source-mute sink_effects_and_mic.monitor 0
	
	
	# 6. On créé les loopback pour entendre dans les hauts parleurs, c'est le wiring effectif
	# On pousse le son du micro (anti-echoed) vers le sink_effects_and_mic
	# On pousse tous les sons envoyés vers sink_effects dans sink_effects_and_mic
	# On pousse tous les sons envoyés vers sink_effects dans les hauts parleurs
	echo "Creating loopbacks"
	pactl load-module module-loopback latency_msec=60 adjust_time=6 source=src_mic_echocancel   sink=sink_effects_and_mic
	pactl load-module module-loopback latency_msec=60 adjust_time=6 source=sink_effects.monitor sink=sink_effects_and_mic
	pactl load-module module-loopback latency_msec=60 adjust_time=6 source=sink_effects.monitor sink=sink_main

	# Mise à dispo d'un loopback pour entendre le micro, mais on le mute par defaut
	#echo "Creating loopbacks"
	#pactl load-module module-loopback latency_msec=60 adjust_time=6 source=src_mic_echocancel   sink=sink_main

	# À la fin, le son du micro et le son des FX (ou toute app qui prend pour carte de sortie sink_effects)
	# vont donc dans ink_effects_and_mic. En (4), on prend le retour (monitor) pour en faire une entrée à part entière (src_main)
	# pour Chrome.
	# La carte son du système par défaut (pour toutes les app qui ne sont pas soundboard) est sink_main
	# Le micro que vous donnez aux applications est src_main
}

# Centralisation d'un potars
reset_potar () {
	# Led en mode "Trim" (part du milieu, s'etend sur les côtés)
	amidi -p "$amidi_hw" -S "B0 $1 04";
	# Valeur a 0x40 = 64
	amidi -p "$amidi_hw" -S "BA $1 40";
}

# Par défaut, ma webcam et/ou chromium activent certains réglages auto, qu'on désactive ici.
default_options() {
	v4l2-ctl -d /dev/video0 --set-ctrl=exposure_auto_priority=0
	v4l2-ctl -d /dev/video0 --set-ctrl=exposure_auto=2
	v4l2-ctl -d /dev/video0 --set-ctrl=white_balance_temperature_auto=0
}

# Loop: centralisation de tous les potars
reset_all_potars () {
	default_options
	for i in $(seq 1 8)
	do
		reset_potar $i
	done
}

# Fonction de jeu de son (vers le sink_effects)
play () {
	sound=$(dirname $0)/sounds-enabled/$1
	echo "Playing $sound"
	#mplayer -quiet -noconsolecontrols $1 &
	killall paplay 2>/dev/null || paplay -d sink_effects $sound &
}

# Routine principale, qui contient le loop pour le midi
launch () {
	# On affiche la date pour la retrouver dans les logs
	echo "Launching $0"
	date
	pwd
	echo "Resetting les potars";
	reset_all_potars
	wire_pulse

	# Loop Midi
	aseqdump -p "X-TOUCH MINI" | while IFS=" ," read src ev1 ev2 ch label1 data1 label2 data2 rest; do
		# Réaction à chaque event
		case "$ev1 $ev2 $data1" in
			"Note off 8" ) play 1.wav ;;
			"Note off 9" ) play 2.wav ;;
			"Note off 10" ) play 3.wav ;;
			"Note off 11" ) play 4.wav ;;
			"Note off 12" ) play 5.wav ;;
			"Note off 13" ) play 6.wav ;;
			"Note off 14" ) play 7.wav ;;
			"Note off 15" ) play 8.wav ;;
			"Note off 16" ) 
				pactl set-source-mute sink_effects_and_mic.monitor toggle
				# Retour dans les logs
				pactl get-source-mute sink_effects_and_mic.monitor
				;;
			"Note off 17" ) ;;
			"Note off 18" ) xdotool key XF86AudioPrev ;;
			"Note off 19" ) xdotool key XF86AudioNext ;;
			"Note off 20" ) ;;
			"Note off 21" ) xdotool key alt+ctrl+q ;;
			"Note off 22" ) xdotool key XF86AudioPlay ;;
			"Note off 23" ) wmctrl -a Spotify || /snap/bin/spotify & ;;
		
			"Note off 0" ) 
				default_options
				reset_potar 1
				v4l2-ctl -d /dev/video0 --set-ctrl=exposure_auto_priority=0 
				v4l2-ctl -d /dev/video0 --set-ctrl=exposure_absolute=1250 
				v4l2-ctl -d /dev/video0 --set-ctrl=exposure_auto=2 
				;;
			"Control change 1" ) 
				v4l2-ctl -d /dev/video0 --set-ctrl=exposure_absolute=$(($data2*2500/127)) 
				;;

			"Note off 1" ) 
				default_options
				reset_potar 2
				v4l2-ctl -d /dev/video0 --set-ctrl=contrast=128 
				;;
			"Control change 2" ) 
				default_options
				v4l2-ctl -d /dev/video0 --set-ctrl=contrast=$(($data2*255/127)) 
				;;
		
			"Note off 2" ) 
				default_options
				reset_potar 3
				v4l2-ctl -d /dev/video0 --set-ctrl=saturation=128 
				;;
			"Control change 3" ) 
				v4l2-ctl -d /dev/video0 --set-ctrl=saturation=$(($data2*255/127)) 
				;;
			
			"Note off 3" ) 
				reset_potar 4
				default_options
				v4l2-ctl -d /dev/video0 --set-ctrl=gain=50 
				;;
			"Control change 4" ) 
				# Ma cam vois pas vraiment un gain <50, autant spread
				v4l2-ctl -d /dev/video0 --set-ctrl=gain=$(($data2*50/127 + 50)) 
				#v4l2-ctl -d /dev/video0 --set-ctrl=gain=$(($data2*100/127)) 
				;;
			
			"Note off 4" ) 
				default_options
				reset_potar 5
				v4l2-ctl -d /dev/video0 --set-ctrl=white_balance_temperature_auto=0 
				v4l2-ctl -d /dev/video0 --set-ctrl=white_balance_temperature=4650 
				;;
			"Control change 5" ) 
				v4l2-ctl -d /dev/video0 --set-ctrl=white_balance_temperature=$((2800+$data2*3700/127)) 
				;;
			
			"Note off 5" ) 
				default_options
				reset_potar 6
				v4l2-ctl -d /dev/video0 --set-ctrl=sharpness=128 
				;;
			"Control change 6" ) 
				v4l2-ctl -d /dev/video0 --set-ctrl=sharpness=$(($data2*255/127)) 
				;;
			
			"Note off 6" ) 
				default_options
				reset_potar 7
				v4l2-ctl -d /dev/video0 --set-ctrl=brightness=128 
				;;
			"Control change 7" ) 
				v4l2-ctl -d /dev/video0 --set-ctrl=brightness=$(($data2*255/127)) 
				;;
			
			"Note off 7" ) 
				default_options
				reset_potar 8 
				;;

			"Note off 8" ) 
				;;
			"Control change 8" ) 
				;;
			
			# Slide à droite de la tablette, change le volume sonore des hauts-parleurs
			"Control change 9" ) 
				pactl set-sink-volume sink_main $(($data2*100/127))% 
				;;
	    esac

		# Allumer les LEDs si besoin
		(wmctrl -l | grep Spotify > /dev/null) && amidi -p "$amidi_hw" -S "9A 17 01" || amidi -p "$amidi_hw" -S "9A 17 00";
		# LED du micro allumé
		mic_open=$(pactl get-source-mute sink_effects_and_mic.monitor | grep oui > /dev/null ; echo $?)
		if [ "$mic_open" = 0 ]
		then
			amidi -p "$amidi_hw" -S '9A 10 00'
		else
			amidi -p "$amidi_hw" -S '9A 10 01'
		fi			
	done
}

# On lance en gardant trace des outputs en guise de logs
launch | tee $(dirname $0)/logs/$(date +%Y%m%d-%H%M).log
