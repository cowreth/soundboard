#! /bin/bash

# This one was my legacy Soundboard on MacOS, where I needed to
# decrease the volume before launching the sound.

osascript -e "set Volume 5"
afplay ~/Music/SB/$1.*
osascript -e "set Volume 0"
