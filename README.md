# My fun manual soundbard

Hardware :
- I use a Behringer X-TOUCH Mini platform, cheap and efficient

Software :
- I can't remember what I had to install. v4l2 is surely a thing. I don't think any pulse component is per-se required, but `pavucontrol` is really helpful (though I think it comes preinstalled with Ubuntu).

Info :
- The "pulseaudio" wiring may break previous configurations
- To get rid of the wiring, just type `pulseaudio -k` in your CLI

How-to :
- First, update the `control_midi.sh` script, to set your own hardware addresses
- Then, fill the `sounds-avalaible/` dir with WAV soundfiles
- Link the soundfiles to `[1-8].wav` files in the `sounds-enabled/` directory
- Make the script to be started with your session (I use the `startup applications` tool in Ubuntu)

Enjoy.

# Bugs ?

Probably.