#!/bin/bash

# Utilisation avec la tablette X-TOUCH MINI de Behringer
# Version avec la tablette en mode Mackie

# Identification de l'identifiant de la platine MIDI
amidi_hw=$(amidi --list-devices | grep "X-TOUCH" | cut -d " " -f 3)
echo "Midi is : " $amidi_hw

# Camera
camera_dev=$(v4l2-ctl --list-devices 2>/dev/null | grep -A1 "PC-W3" | tail -1 | tr -d "\t")
echo "Camera is : " $camera_dev

# Init d'un potar : lights off
reset_potar () {
	# Led éteinte
	amidi -p "$amidi_hw" -S "B0 $1 00";
	echo amidi -p "$amidi_hw" -S "B0 $1 00";
}

# Init d'un bouton : lights off
reset_button () {
	# Led éteinte
	amidi -p "$amidi_hw" -S "90 $1 00";
	echo amidi -p "$amidi_hw" -S "90 $1 00";
}

# Par défaut, ma webcam et/ou chromium activent des réglages auto, qu'on désactive ici
# On va stocker aussi les valeurs dans des variables pour pouvoir les modifier en relatif
# sans avoir à appelr v4l2-ctl à chaque mouvement de potar
default_cam_options() {
	default_exposure_absolute=1205
	current_exposure_absolute=$default_exposure_absolute
	
	default_contrast=113
	current_contrast=$default_contrast
	
	default_saturation=141
	current_saturation=$default_saturation
	
	default_gain=47
	current_gain=$default_gain
	
	default_white_balance_temperature=4450
	current_white_balance_temperature=$default_white_balance_temperature
	
	default_sharpness=128
	current_sharpness=$default_sharpness
	
	default_brightness=128
	current_brightness=$default_brightness
}

# Tool : fonction d'application des paramètres à la cam
apply_cam_options () {
	v4l2-ctl -d $camera_dev \
		--set-ctrl=exposure_auto_priority=0 \
		--set-ctrl=exposure_auto=2 \
		--set-ctrl=white_balance_temperature_auto=0 \
		--set-ctrl=exposure_absolute=$current_exposure_absolute \
		--set-ctrl=contrast=$current_contrast \
		--set-ctrl=saturation=$current_saturation \
		--set-ctrl=gain=$current_gain \
		--set-ctrl=white_balance_temperature=$current_white_balance_temperature \
		--set-ctrl=sharpness=$current_sharpness \
		--set-ctrl=brightness=$current_brightness
}

# Init de la cam
init_camera() {
	default_cam_options
	apply_cam_options
}

# Init de la camera, tous les potars et boutons
reset_all () {
	echo "Resetting all devices"
	
	echo "Resetting webcam"
	init_camera

	echo "Resetting potars"
	# Reset des potars en bouclant sur leur index en hexa
	for i in 30 31 32 33 34 35 36 37
	do
		reset_potar $i
	done

	echo "Resetting buttons"
	# Reset des boutons en bouclant sur leur index en hexa
	for i in 28 29 2A 2B 2C 2D 54 55 56 57 58 59 5A 5B 5C 5D 5E 5F
	do
		reset_button $i
	done
}

# Tool : décoder les valeurs relatives des potars
decode_relative_potar() {
	case "$1" in
		1 )	potar_change_value=1;;
		2 )	potar_change_value=2;;
		3 )	potar_change_value=4;;
		4 )	potar_change_value=8;;
		5 )	potar_change_value=16;;
		6 )	potar_change_value=32;;
		7 )	potar_change_value=64;;
		
		65 )	potar_change_value=-1;;
		66 )	potar_change_value=-2;;
		67 )	potar_change_value=-4;;
		68 )	potar_change_value=-8;;
		69 )	potar_change_value=-16;;
		70 )	potar_change_value=-32;;
		71 )	potar_change_value=-64;;
	esac	
}

# Tool : Fonction de jeu de son (vers le sink_effects)
play () {
	sound=$(dirname $0)/sounds-enabled/$1
	echo "Playing $sound"
	#mplayer -quiet -noconsolecontrols $1 &
	killall paplay 2>/dev/null || paplay -d sink_effects $sound &
}

# Routine principale, qui contient le loop pour le midi
launch () {
	# On affiche la date pour la retrouver dans les logs
	echo "$(date) : Launching $0 in $(pwd)"
		
	reset_all

	# Loop Midi
	# Loop Midi
	aseqdump -p "X-TOUCH MINI" | while IFS=" ," read src ev1 ev2 ch label1 data1 label2 data2 rest; do
		# echo "DEBUG_RAW src:$src ev1:$ev1 ev2:$ev2 ch:$ch label1:$label1 data1:$data1 label2:$label2 data2:$data2 rest:$rest"
		
		# Réaction à chaque event
		case "$ev1 $ev2" in

			# Bouton Appuyé
			"Note off" )
				case "$data1" in
					# Rangée 1 : Soundboard
					89 ) play 1.wav ;;
					90 ) play 2.wav ;;
					40 ) play 3.wav ;;
					41 ) play 4.wav ;;
					42 ) play 5.wav ;;
					43 ) play 6.wav ;;
					44 ) play 7.wav ;;
					45 ) play 8.wav ;;

					# Rangée 2
					87 ) play 9.wav ;;
					88 ) play 10.wav ;;
					91 ) play 11.wav ;;
					92 ) play 12.wav ;;
					86 ) play 13.wav ;;
					93 ) ;;
					94 ) ;;
					95 ) echo "Launch or focus Spotify" ; wmctrl -a Spotify || /snap/bin/spotify & ;;
					
					# Layer A : Micro coupé / allumé
					84 ) 
						pactl set-source-mute sink_effects_and_mic.monitor toggle
						# Retour dans les logs
						pactl get-source-mute sink_effects_and_mic.monitor
						;;
					# Layer B
					85 ) 
						;;
					
					# Potar 1 : reset de la caméra
					32 )
						default_cam_options
						apply_cam_options
						;;
				esac
			;;
			
			# Potar Tourné
			"Control change" )
				decode_relative_potar $data2
				#echo "DEBUG Potar : $data1 $data2 $potar_change_value" ; 

				case "$data1" in
					# Min/Max à checker avec v4l2-ctl -d /dev/video0 -l
					16 ) 
						# Je fais un x10 sur la valeur à ajouter parce que le range de exposition est très large
						current_exposure_absolute=$(($current_exposure_absolute+potar_change_value*10))
						if [ $current_exposure_absolute -lt 5 ]; then current_exposure_absolute=5; fi
						if [ $current_exposure_absolute -gt 2500 ]; then current_exposure_absolute=2500; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=exposure_absolute=$current_exposure_absolute \
						;;
					17 ) 
						current_contrast=$(($current_contrast+potar_change_value))
						if [ $current_contrast -lt 1 ]; then current_contrast=1; fi
						if [ $current_contrast -gt 255 ]; then current_contrast=255; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=contrast=$current_contrast \
						;;
					18 ) 
						current_saturation=$(($current_saturation+potar_change_value)) 
						if [ $current_saturation -lt 1 ]; then current_saturation=1; fi
						if [ $current_saturation -gt 255 ]; then current_saturation=255; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=saturation=$current_saturation \
						;;
					19 ) 
						current_gain=$(($current_gain+potar_change_value)) 
						if [ $current_gain -lt 1 ]; then current_gain=1; fi
						if [ $current_gain -gt 100 ]; then current_gain=100; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=gain=$current_gain \
						;;
					20 ) 
						# Je fais un x50 sur la valeur à ajouter parce que le range de white_balance est très large
						current_white_balance_temperature=$(($current_white_balance_temperature+potar_change_value*50)) 
						if [ $current_white_balance_temperature -lt 2800 ]; then current_white_balance_temperature=2800; fi
						if [ $current_white_balance_temperature -gt 6500 ]; then current_white_balance_temperature=6500; fi
						
						# Chromium réactive l'expo auto de la camra, donc on doit aussi la redésactiver
						v4l2-ctl -d $camera_dev \
							--set-ctrl=white_balance_temperature_auto=0 \
							--set-ctrl=white_balance_temperature=$current_white_balance_temperature \
						;;
					21 ) 
						current_sharpness=$(($current_sharpness+potar_change_value)) 
						if [ $current_sharpness -lt 1 ]; then current_sharpness=1; fi
						if [ $current_sharpness -gt 255 ]; then current_sharpness=255; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=sharpness=$current_sharpness \
						;;
					22 ) 
						current_brightness=$(($current_brightness+potar_change_value))
						if [ $current_brightness -lt 1 ]; then current_brightness=1; fi
						if [ $current_brightness -gt 255 ]; then current_brightness=255; fi
						v4l2-ctl -d $camera_dev \
							--set-ctrl=brightness=$current_brightness
						;;
					23 ) 
						;;

				esac
				
			;;
			
			# Le Fader
			"Pitch bend" )
				;;
	    esac

		# Allumer les LEDs si besoin
		(wmctrl -l | grep Spotify > /dev/null) && amidi -p "$amidi_hw" -S "90 5F 7F" || amidi -p "$amidi_hw" -S "90 5F 00";

		# LED du micro (LAYER A) allumé
		mic_open=$(pactl get-source-mute sink_effects_and_mic.monitor | grep oui > /dev/null ; echo $?)
		if [ "$mic_open" = 0 ]
		then
			amidi -p "$amidi_hw" -S '90 54 7F'
		else
			amidi -p "$amidi_hw" -S '90 54 00'
		fi		
	done
}

# On supprime toutes les instances en cours
killall $0

# On lance en gardant trace des outputs en guise de logs
launch | tee $(dirname $0)/logs/$(date +%Y%m%d-%H%M).log
